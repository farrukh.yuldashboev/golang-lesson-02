package main

import "fmt"

func main() {
	DisplayNumberInReverseOrderWithDefer()
}

func DisplayNumberInReverseOrderWithDefer() (result int) {
	for i := 0; i < 100; i++ {
		defer func() {
			result = i
		}()
		fmt.Println(i)
	}
	return
}
