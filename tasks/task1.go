package main

import "fmt"

func main() {
	for i := 1; i <= 100; i++ {
		FizzBuzz(i)
	}
}

func FizzBuzz(num int) {
	switch {
	case num%15 == 0:
		fmt.Println("FizzBuzz")
	case num%3 == 0:
		fmt.Println("Fizz")
	case num%5 == 0:
		fmt.Println("Buzz")
	default:
		fmt.Println(num)
	}
	// if num%15 == 0 {
	// 	fmt.Println("FizzBuzz")
	// } else if num%3 == 0 {
	// 	fmt.Println("Fizz")
	// } else if num%5 == 0 {
	// 	fmt.Println("Buzz")
	// } else {
	// 	fmt.Println(num)
	// }

}
