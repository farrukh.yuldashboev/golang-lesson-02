package main

import (
	"fmt"
)

func main() {
	n := 11
	// Read n from input
	fmt.Println(DisplayMinimumNumberFunction(n))
}

// https://www.hackerrank.com/contests/w30/challenges/find-the-minimum-number
func DisplayMinimumNumberFunction(n int) (result string) {

	if n == 1 {
		return "min(int, int)"
	}
	result = "min(int, " + (DisplayMinimumNumberFunction(n - 1)) + ")"
	return
}
